<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>NeoTech > Congratulations!</h1>

        <p class="lead">You have successfully installed SIMPLIFIED RESTfull api service and client using Vue.js + Yii 2 + Docker.</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Contact</h2>

                <p>
                    Vladimir Kjahrenov<br>
                    Tel. +372 55 621 935<br>
                    info@hurtlocker.pro<br>
                    Skype: hurtlockerpro
                </p>

            </div>

        </div>

    </div>
</div>

# NeoTech test exercise (for applying for a job)

This is a simple RESTful api (only GET method added) and client for consuming this api service.

**Application work in Chrome, Edge**

This project use technologies:
* Yii 2 php framework (www.yiiframework.com)
* Vue.js (https://vuejs.org)
* Docker (https://www.docker.com)
* Composer (http://getcomposer.org)

INSTALLATION
------------

### Clone from gitlab
* git clone https://gitlab.com/hurtlockerpro/neotech.git neotech

### Install app via Composer

Open command promt (cmd) and go to the neotech folder type: 
~~~
cd neotech
~~~
then install all libraries using composer 
~~~
composer install
~~~

### Install and run Docker containers (MySQL, PhpMyAdmin, Apache server)

Type in cmd: 
~~~
docker-compose up
~~~

### Migrate database and all data into tables 

Run migrate in docker container. For this you need to enter docker container and run migration from inside. 
NB! check container name (docker ps) if apache container differ from name "neotech_php_1" then change it in following string 

~~~
docker exec -it neotech_php_1 bash
~~~

run migration, type: 

~~~
php yii migrate
~~~

### Run application

If you see successfull message then everything is ok and you can use service.

~~~
http://localhost:8000/apiclient
~~~




### Contact

* Vladimir Kjahrenov
* Tel. + 372 55 621 935
* info@hurtlocker.pro
* Skype: hurtlockerpro


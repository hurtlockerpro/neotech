<?php


namespace app\modules\v1\controllers;


use Yii;
use yii\rest\ActiveController;


class CityController extends ActiveController
{
    public $modelClass = 'app\modules\v1\models\City';

    public function actionStatistics()
    {

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT ctr.Continent as continent, ctr.Region as region,  
(SELECT count(Code) FROM `Country` where `Country`.Region = ctr.Region group by Continent, Region ) as countries, 
(SELECT COUNT(Language) FROM `CountryLanguage` where `CountryLanguage`.`CountryCode` IN (SELECT Code FROM `Country` where `Country`.Region = ctr.Region)) as languages,
COUNT(ct.CountryCode) as cities, AVG(ctr.LifeExpectancy) as lifeduration FROM `Country` AS ctr
left join City as ct on ctr.Code = ct.CountryCode
group by ctr.Continent, ctr.Region
order by ctr.Continent, ctr.Region ASC");

        $result = $command->queryAll();

        return $result;
    }

}